#include <stdlib.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <getopt.h>
#include <pthread.h>

typedef char* command_t;

typedef struct circbuf {
	size_t elements;
	size_t capacity;
	size_t start;
	size_t end;
	command_t *data;
} circbuf_t;

void circdisplay(circbuf_t const* buffer) {
	printf("[");
	for (
		size_t i = buffer-> start; 
		i != buffer-> end; 
		i = (i + 1) % buffer->capacity
	) {
		printf("%s ", buffer->data[i]);
	}
	printf("\b]\n");
}

int circempty(circbuf_t const* buffer) {
	return buffer->elements == 0;
}

int circfull(circbuf_t const* buffer) {
	return buffer->elements == buffer->capacity;
}

int circput(circbuf_t *buffer, command_t cmd) {
	if (!circfull(buffer)) {
		buffer->data[buffer->end++] = cmd;
		buffer->end %= buffer->capacity; 
		buffer->elements++;
	}

	return buffer->capacity - buffer->elements;
}

command_t circget(circbuf_t *buffer) {
	if (circempty(buffer)) 
		return NULL;

	command_t cmd  = buffer->data[buffer->start++];
	buffer->start %= buffer->capacity;
	buffer->elements--;

	return cmd;
}

int process_exists(pid_t pid) {
	if (pid == 0)
		return 0;
	else 
		return getpgid(pid) >= 0;
}

command_t get_command(
	int *chars_read, 
	char const* pref, 
	char const* post, 
	FILE* file
) {
	command_t cmd = NULL;
	size_t size = 0;
	*chars_read = getline(&cmd, &size, file);

	// remove the newline.
	cmd[strlen(cmd)-1] = 0;

	char *command = calloc (
		strlen(pref) + 
		strlen(post) + 
		strlen(cmd)  + 1,
		sizeof(char)
	);

	strcpy(command, pref);
	strcat(command, cmd);
	strcat(command, post);

	free(cmd);

	return command;
}

void* dispatcher(void* ptr) {
	circbuf_t *buffer = ptr;
	pid_t job = 0;
	while(1 == 1) {
		if (!process_exists(job) && !circempty(buffer)) {
			command_t cmd = circget(buffer);
			
			system(cmd);
			free(cmd);
		} else {
			sleep(1);
		}
	}
}

typedef struct {
	char const* file;
	char const* pref;
	char const* post;

	int buffer_length;

	unsigned asked_for_help:	1;
	unsigned asked_for_version:	1;
	unsigned continue_after_file:	1;
	unsigned padding:		5;
} settings_t;

settings_t parse_cliarg(int argc, char** argv) {
	static struct option longopts[] = {
		{"buffer-len",	required_argument,	NULL, 'b'},
		{"file",	required_argument,	NULL, 'f'},
		{"prefix",	required_argument,	NULL, 'p'},
		{"postfix",	required_argument,	NULL, 'P'},
		{"help",	no_argument,		NULL, 'h'},
		{"version",	no_argument,		NULL, 'v'},
		{"continue",	no_argument,		NULL, 'c'},
		{0,0,0,0}
	};

	static char const* argspec = "b:f:p:P:hsSvc";

	settings_t result = {0};
	result.buffer_length = 128;

	while (1 == 1) {
		int option_index;
		int option_letter = getopt_long(
			argc, argv, 
			argspec, longopts, 
			&option_index
		);

		if (option_letter == -1)
			break;

		switch (option_letter) {
		break; case 'b': 
			result.buffer_length = atoi(optarg);
		break; case 'f':
			result.file = optarg;
		break; case 'p':
			result.pref = optarg;
		break; case 'P':
			result.post = optarg;
		break; case 'h':
			result.asked_for_help = 1;
		break; case 'v': 
			result.asked_for_version = 1;
		break; case 'c':
			result.continue_after_file = 1;
		case '?': default:
			abort();
		}
	}

	return result;
}

int main (int argc, char** argv) {
	settings_t settings = parse_cliarg(argc, argv);

	if (settings.asked_for_help) {
		printf(
			"Usage: queuesh [OPTIONS]\n"
			"For more information, refer to queuesh(1).\n"
		);
		exit(0);
	}

	if (settings.asked_for_version) {
		printf(
			"queuesh 0.1.1\n"
			"Copyright (C) 2022 José Goudet Alvim.\n"
			"This is free software; "
			"see the source for copying conditions.\n"
			"There is NO warranty; " 
			"not even for MERCHANTABILITY or "
			"FITNESS FOR A PARTICULAR PURPOSE\n"
		);
		exit(0);
	}

	circbuf_t buffer  = {
		.elements = 0,
		.capacity = settings.buffer_length,
		.start = 0,
		.end   = 0,
		.data  = calloc (
			settings.buffer_length, 
			sizeof(command_t)
		)
	};

	pthread_t dispatcher_thread; 
	pthread_create (
		&dispatcher_thread,
		NULL, 
		dispatcher,
		(void*)&buffer
	);

	FILE* file;
	if (settings.file == NULL) {
		file = stdin;
	} else { 
		file = popen(settings.file, "r");
	}

	while (1 == 1) {
		if (!circfull(&buffer)) {
			if (file == stdin)
				printf("$ ");
			
			int num;
			command_t cmd = get_command(
				&num, 
				settings.pref, 
				settings.post, 
				file
			);
			
			if (
				file != stdin && 
				num <= 0 && 
				settings.continue_after_file
			) {
				file = stdin;
				continue;
			}

			if (num <= 0) {
				pthread_join(dispatcher_thread, NULL);
				break;
			}

			if (cmd[0] == '\n') {
				free(cmd);
				continue;
			}
			
			circput(&buffer, cmd);
		} else {
			if (file == stdin)
				printf("\x1b[2Kbuffer full, please wait");
			sleep(1);
		}
	}
	
	if (file != stdin) {
		pclose(file);
	}

	for (
		size_t i = buffer.start;
		i != buffer.end;
		i = (i+1)%buffer.capacity
	) {
		free(buffer.data[i]);
	}

	free(buffer.data);

	return 0;
}
