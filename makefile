PREFIX ?= /usr/local
CC     ?= gcc
FLAGS  += -O3

.PHONY: build install uninstall

build: queuesh
	@ echo done!

uninstall:
	rm ${PREFIX}/bin/queuesh
	rm ${PREFIX}/man/man1/queuesh.1

install: queuesh queuesh.troff
	install queuesh       ${PREFIX}/bin/queuesh
	install queuesh.troff ${PREFIX}/man/man1/queuesh.1

queuesh: queuesh.c
	${CC} -std=c17 ${FLAGS} -pthread -o $@ $<
