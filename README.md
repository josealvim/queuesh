# QUEUESH
Queuesh is a a command line tool for automatically managing 
a command queue that can be appended by the user. Currently
it only supports a queue of size 10, but that could easily 
be changed by the user when compiling. I might add an 
option and a more sensible default for that later.

A friendly reminder, you can manage the garbage your 
commands output redirecting `stderr` and `stdout` to, say,
`/dev/null` or to some log file if those interest you. 
This, obviously, makes the interactive experience better.

# Installation
## Requirements/Dependencies 
- It should work on POSIX/\*NIX systems without any 
  additional dependencies. You need `make` and `gcc` or 
  something equivalent. 

## Building
- `make build` just to build, or `sudo make install` for 
  system installation.

# Documentation
See the associated manpage

# License and Legal Stuff
This work is licensed under GNU GPL v3.0. 
It is important to remark.
 
	15. Disclaimer of Warranty.

	  THERE IS NO WARRANTY FOR THE PROGRAM,  TO THE EXTENT PERMITTED 
	BY APPLICABLE LAW.   EXCEPT WHEN OTHERWISE STATED IN WRITING THE 
	COPYRIGHT HOLDERS AND/OR  OTHER PARTIES  PROVIDE THE PROGRAM "AS 
	IS" WITHOUT WARRANTY OF ANY  KIND,  EITHER EXPRESSED OR IMPLIED, 
	INCLUDING,  BUT  NOT  LIMITED  TO,  THE  IMPLIED  WARRANTIES  OF 
	MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE ENTIRE
	RISK AS  TO THE QUALITY  AND PERFORMANCE OF THE PROGRAM  IS WITH
	YOU.  SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF
	ALL NECESSARY SERVICING, REPAIR OR CORRECTION.

	16. Limitation of Liability.

	  IN NO EVENT UNLESS REQUIRED BY  APPLICABLE LAW OR AGREED TO IN 
	WRITING  WILL  ANY COPYRIGHT HOLDER,  OR  ANY  OTHER  PARTY  WHO 
	MODIFIES  AND/OR  CONVEYS  THE  PROGRAM  AS  PERMITTED ABOVE, BE 
	LIABLE TO  YOU  FOR  DAMAGES,  INCLUDING ANY GENERAL,   SPECIAL, 
	INCIDENTAL  OR  CONSEQUENTIAL  DAMAGES ARISING OUT OF THE USE OR 
	INABILITY TO USE THE PROGRAM (INCLUDING BUT  NOT LIMITED TO LOSS 
	OF DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY 
	YOU OR THIRD PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH 
	ANY OTHER PROGRAMS), EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN 
	ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.

